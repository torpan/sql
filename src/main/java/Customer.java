public class Customer {
    private String CustomerId;
    private String FirstName;
    private String LastName;

    public Customer(String CustomerId, String FirstName, String LastName){
        this.CustomerId = CustomerId;
        this.FirstName = FirstName;
        this.LastName = LastName;
    }

    //I had a getter for Id here to, but after working through the steps in the task, I didn't need it in the end.

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }
}