import java.sql.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the CustomerID for the customer you want to look at.");
        String userInput = sc.nextLine();
        int number;
        Random rand = new Random();

        //This will not work if someone types a letter, but I choose to not focus on that for this task
        if(!userInput.isEmpty()){
            number = Integer.parseInt(userInput);
        }
        else{
            number = rand.nextInt(58);
        }

        //Setup
        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        Connection conn = null;
        ArrayList<Customer> singlecustomers = new ArrayList<Customer>();
        ArrayList<Genre> genres = new ArrayList<Genre>();

        try {
            //Open connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established");

            //Prepare Statement
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName " +
                    "FROM Customer WHERE CustomerId =" + number);
            //Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();


            //Process results
            while (resultSet.next()) {
                singlecustomers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName")
                        ));
            }
            for (Customer customer : singlecustomers) {
                System.out.println("Customer: "+customer.getFirstName() + " " + customer.getLastName());
            }

            PreparedStatement preparedStatement2 = conn.prepareStatement("SELECT COUNT(genre.Name), genre.Name " +
                    "FROM customer, invoice, invoiceline, track, genre " +
                    "WHERE customer.customerId=invoice.customerId " +
                    "AND invoice.InvoiceId=invoiceline.InvoiceId " +
                    "AND invoiceline.trackId=track.trackId " +
                    "AND track.genreId=genre.genreId " +
                    "AND customer.CustomerId= "+ number+" GROUP BY genre.Name ORDER BY COUNT(genre.Name) DESC");

            ResultSet resultSet2 = preparedStatement2.executeQuery();

            while (resultSet2.next()) {
                genres.add(
                        new Genre(
                                resultSet2.getInt(1),
                                resultSet2.getString("Name")
                        ));
            }
                //In case the user (or the random number) picks the customer with 2 equally popular genres, it will print out both of them.
                //Since i picked descending in my SQL-guery, I just print out the first one in my ArrayList.
                if(genres.get(0).getGenreId() != genres.get(1).getGenreId()){
                    System.out.println("Most popular genre: "+genres.get(0).getGenreName());
                }
                else if (genres.get(0).getGenreId() == genres.get(1).getGenreId()){
                    System.out.println("Most popular genres: \n"+genres.get(0).getGenreName()+" and "+genres.get(1).getGenreName());
                }

            }
            catch (Exception ex) {
                System.out.println("Something went wrong...");
                System.out.println(ex.toString());
            }
            finally {
                try {
                    //Close connection
                    conn.close();
                    }
            catch (Exception ex) {
                System.out.println("Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
    }
}
