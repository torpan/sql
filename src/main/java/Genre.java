public class Genre {
    private int GenreId;
    private String GenreName;

    public Genre(int GenreId, String GenreName){
        this.GenreId = GenreId;
        this.GenreName = GenreName;
    }
    public int getGenreId() {
        return GenreId;
    }
    public String getGenreName() {
        return GenreName;
    }
}
